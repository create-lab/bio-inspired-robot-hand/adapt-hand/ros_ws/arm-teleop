from setuptools import find_packages, setup

package_name = 'arm_teleop'

setup(
    name=package_name,
    version='0.0.0',
    packages=find_packages(exclude=['test']),
    data_files=[
        ('share/ament_index/resource_index/packages',
            ['resource/' + package_name]),
        ('share/' + package_name, ['package.xml']),
    ],
    install_requires=['setuptools'],
    zip_safe=True,
    maintainer='kaijunge',
    maintainer_email='kai.junge@epfl.ch',
    description='TODO: Package description',
    license='Apache-2.0',
    tests_require=['pytest'],
    entry_points={
        'console_scripts': [
            'arm_teleop_node = arm_teleop.arm_teleop_node:main',
            'orientation_node = arm_teleop.orientation_node:main',
            'translation_node = arm_teleop.translation_node:main'
        ],
    },
)
