from geometry_msgs.msg import PoseStamped
from helper_functions.gamepad_functions import GamepadFunctions

import numpy as np
import cv2
import math
import time
from sensor_msgs.msg import Joy

from .teleop_helper_functions import *

import rclpy
from rclpy.node import Node

from copy import deepcopy as cp

class TranslationNode(Node):

    def __init__(self):
        super().__init__('translation_node')

        # Gamepad
        self.gp = GamepadFunctions()
        self.gamepad_subscriber = self.create_subscription(Joy, '/gamepad', self.gamepad_callback, 10)

        # Main loop
        self.mainloop = self.create_timer(0.01, self.mainloop_callback) 
        self.cameraloop = self.create_timer(0.001, self.camearaloop_callback) 

        # Publisher
        self.target_translation_publisher = self.create_publisher(PoseStamped, '/franka/translation_demand', 10)

        # Subscriber
        self.franka_cartesian_pose_subscriber = self.create_subscription(PoseStamped, '/cartesian_pose', self.cart_pos_callback, 10)

        # Variables
        self.verified_raw_positions = None
        self.damped_raw_positions = [0.0, 0.0, 0.0]
        self.last_update = cp(time.time())
        self.current_franka_pose = None
        
        # True/False flags
        self.is_active = False
        
        # Parameters
        self.DAMPING = 0.9
        self.SCALING = [0.8, 0.8, 0.8]

        self.zed, self.runtime, self.image_size, self.image_zed, self.depth_image_zed, self.point_cloud = init_zed_camera()


    def update_zed_position(self, raw_position):
        # Check absolute values
        if abs(raw_position[0]) > 600 or abs(raw_position[1]) > 200 or raw_position[2] > 900 or raw_position[2] < 400:
            return False
            
        # Check difference with existing data
        if self.verified_raw_positions is None:
            self.verified_raw_positions = cp(raw_position); self.last_update = cp(time.time())
            return True
        else:
            if self.is_last_update_recent():
                for i in range(3):
                    if abs(self.verified_raw_positions[i] - raw_position[i]) > 50:
                        return False
                self.verified_raw_positions = cp(raw_position); self.last_update = cp(time.time())
                return True
            else:
                self.verified_raw_positions = cp(raw_position); self.last_update = cp(time.time())
                return True

        
    def is_last_update_recent(self, threshold = 0.5):
        if time.time() - self.last_update < threshold:
            return True
        else:
            return False

    def camearaloop_callback(self):
        err = self.zed.grab(self.runtime)
        if err == sl.ERROR_CODE.SUCCESS :
            self.zed.retrieve_image(self.image_zed, sl.VIEW.LEFT, sl.MEM.CPU, self.image_size)
            self.zed.retrieve_image(self.depth_image_zed, sl.VIEW.DEPTH, sl.MEM.CPU, self.image_size)
            # Retrieve the RGBA point cloud in half resolution
            self.zed.retrieve_measure(self.point_cloud, sl.MEASURE.XYZRGBA, sl.MEM.CPU, self.image_size)


            # Get image and then blur
            raw = self.image_zed.get_data()
            blur = cv2.blur(raw, (6,6))

            # Threshold HSV
            hsv = cv2.cvtColor(blur, cv2.COLOR_BGR2HSV)
            lower = np.array([10, 200, 200]) 
            upper = np.array([40, 255, 255])  
            mask = cv2.inRange(hsv, lower, upper)

            kernel = np.ones((3, 3), np.uint8)
            dilate = cv2.dilate(mask, kernel, iterations=1)
            erode = cv2.erode(dilate, kernel, iterations=1)

            contours,_ = cv2.findContours(erode, 1, 2)

            is_detected = False
            contour_info = [-10, None]
            for cnt in contours:
                if cv2.contourArea(cnt) > 600 and cv2.contourArea(cnt) > contour_info[0]:
                    contour_info = [cv2.contourArea(cnt), cnt]
                    is_detected = True

            if is_detected:
                M = cv2.moments(contour_info[1])
                cX = int(M["m10"] / M["m00"]);  cY = int(M["m01"] / M["m00"])
                cv2.circle(raw, (cX, cY), 7, (255, 255, 255), -1)
            
                loop = 10
                count = 0
                raw_position = [0.0, 0.0, 0.0]
                for i in range(2):
                    for x in range(loop):
                        for y in range(loop):
                            try:
                                pc = self.point_cloud.get_value(cX + (1-i*2) * x, cY + (1-i*2) * y)
                                point_cloud_value = pc[1]
                                if math.isnan(point_cloud_value[0]) == False:
                                    for i in range(3):
                                        raw_position[i] += point_cloud_value[i]
                                    count += 1
                            except:
                                print("Pixel out of range!")
                if count > 0:
                    for i in range(3):
                        raw_position[i]= round(raw_position[i] / count)

                # Check validity of the data
                is_updated = self.update_zed_position(raw_position)   
                if is_updated:
                    print_msg = "Update YES: "
                else:
                    print_msg = "Update NO: "
                
                print_msg += str(raw_position)
                self.get_logger().info(print_msg, throttle_duration_sec = 0.3)

            # Display the hue objects
            cv2.imshow('hue', erode)
            cv2.imshow("raw", raw)
            cv2.waitKey(1)


    def cart_pos_callback(self, msg:PoseStamped):
        self.current_franka_pose = msg

    def gamepad_callback(self, gamepad_raw_msg:Joy):
        self.gp.convert_joy_msg_to_dictionary(gamepad_raw_msg)

    def mainloop_callback(self):
        
        # If we go from button not pressed to button pressed.
        if self.gp.button_data["L1"] == True and self.is_active == False:

            # If the camera is seeing good data
            if self.is_last_update_recent():
                self.start_zed_pose = cp(self.verified_raw_positions)
                self.start_franka_pose = cp(self.current_franka_pose)
                self.damped_raw_positions = [0.0, 0.0, 0.0]
                self.is_active = True

        # If we let go of the button
        if self.gp.button_data["L1"] == False and self.is_active == True:
            self.is_active = False

        # If activity button pressed
        if self.is_active:
            raw = cp(self.verified_raw_positions)
            demand_translation = [0.0, 0.0, 0.0]

            for i in range(3):
                self.damped_raw_positions[i] = self.damped_raw_positions[i] * self.DAMPING + (raw[i] - self.start_zed_pose[i]) * (1-self.DAMPING)
            
            print_msg = str([round(self.damped_raw_positions[0]), round(self.damped_raw_positions[1]), round(self.damped_raw_positions[2])])
            self.get_logger().info(print_msg, throttle_duration_sec = 0.3)

            target_pose = cp(self.start_franka_pose)
            target_pose.pose.position.x += self.damped_raw_positions[1] * -1 * self.SCALING[0] * 0.001
            target_pose.pose.position.y += self.damped_raw_positions[0] * -1 * self.SCALING[1] * 0.001
            target_pose.pose.position.z += self.damped_raw_positions[2] * -1 * self.SCALING[2] * 0.001

            self.target_translation_publisher.publish(target_pose)

def main():
    rclpy.init()
    node = TranslationNode()
    try:
        rclpy.spin(node)
    except KeyboardInterrupt:
        pass

    rclpy.shutdown()


