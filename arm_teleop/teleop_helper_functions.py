import sys
import pyzed.sl as sl
from geometry_msgs.msg import TransformStamped, PoseStamped
import csv
import os


def posestamp_message_as_str(msg:PoseStamped):
    print_msg = ""
    print_msg += "x: " + str(round(msg.pose.position.x, 4)) + "\n"
    print_msg += "y: " + str(round(msg.pose.position.y, 4)) + "\n"
    print_msg += "z: " + str(round(msg.pose.position.z, 4)) + "\n\n"
    
    print_msg += "qx: " + str(round(msg.pose.orientation.x, 4)) + "\n"
    print_msg += "qy: " + str(round(msg.pose.orientation.y, 4)) + "\n"
    print_msg += "qz: " + str(round(msg.pose.orientation.z, 4)) + "\n"
    print_msg += "qw: " + str(round(msg.pose.orientation.w, 4)) + "\n"
    
    return print_msg

def broadcast_tf(self, base_name, frame_name, position, quaternion):
    t = TransformStamped()
    t.header.stamp = self.get_clock().now().to_msg()
    t.header.frame_id = base_name
    t.child_frame_id = frame_name

    t.transform.translation.x = position[0]
    t.transform.translation.y = position[1]
    t.transform.translation.z = position[2]

    t.transform.rotation.x = quaternion[0]
    t.transform.rotation.y = quaternion[1]
    t.transform.rotation.z = quaternion[2]
    t.transform.rotation.w = quaternion[3]

    self.tf_broadcaster.sendTransform(t)
    
    return t

def init_zed_camera():
    # Create a ZED camera object
    zed = sl.Camera()

    # Set configuration parameters
    input_type = sl.InputType()
    if len(sys.argv) >= 2 :
        input_type.set_from_svo_file(sys.argv[1])
    init = sl.InitParameters(input_t=input_type)
    init.camera_resolution = sl.RESOLUTION.HD1080
    init.depth_mode = sl.DEPTH_MODE.PERFORMANCE
    init.coordinate_units = sl.UNIT.MILLIMETER

    # Open the camera
    err = zed.open(init)
    if err != sl.ERROR_CODE.SUCCESS :
        print(repr(err))
        zed.close()
        exit(1)

    # Set runtime parameters after opening the camera
    runtime = sl.RuntimeParameters()

    # Prepare new image size to retrieve half-resolution images
    image_size = zed.get_camera_information().camera_configuration.resolution
    image_size.width = image_size.width /2
    image_size.height = image_size.height /2

    # Declare your sl.Mat matrices
    image_zed = sl.Mat(image_size.width, image_size.height, sl.MAT_TYPE.U8_C4)
    depth_image_zed = sl.Mat(image_size.width, image_size.height, sl.MAT_TYPE.U8_C4)
    point_cloud = sl.Mat()

    return zed, runtime, image_size, image_zed, depth_image_zed, point_cloud




def save_task_startingPose(csv_name, pose):
    csv_content = [[pose.pose.position.x, pose.pose.position.y, pose.pose.position.z,
                    pose.pose.orientation.x, pose.pose.orientation.y, pose.pose.orientation.z, pose.pose.orientation.w]]
    _write_csv(csv_name, csv_content)


parent_dir = os.path.dirname(os.path.realpath(__file__))
dir_name = "/starting_pose"
def _write_csv(csv_name, csv_content:list):
    _save_dir = parent_dir + dir_name
    if not os.path.exists(_save_dir):
        os.makedirs(_save_dir)
    csv_file = _save_dir + "/" + csv_name + ".csv"
    with open(csv_file, 'w', newline='') as file:
        writer = csv.writer(file)
        writer.writerows(csv_content)
        

def load_task_startingPose(csv_name):
    csv_file = parent_dir + dir_name + "/" + csv_name + ".csv"
    wp = []
    with open(csv_file, 'r') as file:
        csvreader = csv.reader(file)
        for row in csvreader:
            row2 = []
            for number in row:
                row2.append(float(number))
            wp.append(row2)
    # print("Loaded starting pose: ", wp[0])
    return wp[0]