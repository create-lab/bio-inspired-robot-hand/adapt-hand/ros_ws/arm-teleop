from geometry_msgs.msg import PoseStamped
from helper_functions.gamepad_functions import GamepadFunctions

from .teleop_helper_functions import posestamp_message_as_str
import time
from sensor_msgs.msg import Joy

from .teleop_helper_functions import *

import rclpy
from rclpy.node import Node

from copy import deepcopy as cp
from scipy.spatial.transform import Slerp
from scipy.spatial.transform import Rotation as R
import numpy as np


class ArmTeleopNode(Node):

    def __init__(self):
        super().__init__('arm_teleop_node')

        # Gamepad
        self.gp = GamepadFunctions()
        self.gamepad_subscriber = self.create_subscription(Joy, '/gamepad', self.gamepad_callback, 10)

        # Main loop
        self.mainloop = self.create_timer(0.001, self.mainloop_callback) 

        # Publisher
        self.franka_pose_publisher = self.create_publisher(PoseStamped, '/equilibrium_pose', 10)

        # Subscriber
        self.franka_cartesian_pose_subscriber = self.create_subscription(PoseStamped, '/cartesian_pose', self.cart_pos_callback, 10)
        self.target_translation_subscriber = self.create_subscription(PoseStamped, '/franka/translation_demand', self.translation_callback, 10)
        self.target_orientation_subscriber = self.create_subscription(PoseStamped, '/franka/orientation_demand',self.orientation_callback, 10)

        # Variables
        self.translation_demand = None
        self.orientation_demand = None
        self.current_franka_pose = None

        self.last_translation_update = cp(time.time())
        self.last_orientation_update = cp(time.time())

        # Flags
        self.is_active = False
        self.reach_starting_pose = False
        self.timer_back = None

    def check_timer(self, which_axis, threshold = 0.5):
        if which_axis == "translation":
            which_timer = self.last_translation_update
        else:
            which_timer = self.last_orientation_update

        if time.time() - which_timer < threshold:
            return True
        else:
            return False
        
    def translation_callback(self, msg:PoseStamped):
        self.translation_demand = msg
        self.last_translation_update = cp(time.time())

    def orientation_callback(self, msg:PoseStamped):
        self.orientation_demand = msg
        self.last_orientation_update = cp(time.time())

    def cart_pos_callback(self, msg:PoseStamped):
        self.current_franka_pose = msg

    def gamepad_callback(self, gamepad_raw_msg:Joy):
        self.gp.convert_joy_msg_to_dictionary(gamepad_raw_msg)
    

    def mainloop_callback(self):
        if self.current_franka_pose is not None:
            if self.gp.button_data["L1"] == 1 and self.is_active == False:
                self.initial_pose = cp(self.current_franka_pose)
                self.is_active = True
                self.waiting_timer = cp(time.time())

            if self.gp.button_data["L1"] == 0:
                self.is_active = False

            if self.gp.button_data["R1"] == 1:
                self.is_active = False
                # save the current pose as csv file
                
                save_task_startingPose(csv_name="pen_starting_pose", pose=cp(self.current_franka_pose))
                self.get_logger().info("Task Starting Pose Saved", throttle_duration_sec = 0.1)
            
            if self.gp.button_data["R2"] == 0:
                self.reach_starting_pose = False
                self.timer_back = time.time()
                
            if self.gp.button_data["R2"] == 1:
                self.is_active = False
                # move robot arm to task starting pose
                loaded_starting_pose = load_task_startingPose(csv_name="box_starting_pose_using")
                
                current_pose = cp(self.current_franka_pose)
                target_pose = cp(self.current_franka_pose)

                def slerp_orient(damping, current_orientation, target_orientation):
                    # print("current_orientation ", current_orientation)
                    # print("target_orientation ", target_orientation)
                    # print("")
                    current_orientation = np.array([current_orientation.x, current_orientation.y, current_orientation.z, current_orientation.w])
                    slerp = Slerp([0, 1], R.from_quat([current_orientation, target_orientation]))
                    target_R = slerp([damping])
                    target_orientation = target_R.as_quat()[0]

                    return target_orientation

                step_t_len = 1000
                pressing_time = time.time() - self.timer_back

                if (not self.reach_starting_pose) and (pressing_time > 2): # need pressing the button at least 2 seconds
                    for step_t in range(step_t_len):
                        target_pose.pose.position.x += (loaded_starting_pose[0] - current_pose.pose.position.x)/step_t_len
                        target_pose.pose.position.y += (loaded_starting_pose[1] - current_pose.pose.position.y)/step_t_len
                        target_pose.pose.position.z += (loaded_starting_pose[2] - current_pose.pose.position.z)/step_t_len

                        temp_orientation = slerp_orient( step_t/step_t_len , current_pose.pose.orientation, np.array(loaded_starting_pose[3:]))
                        target_pose.pose.orientation.x = temp_orientation[0]
                        target_pose.pose.orientation.y = temp_orientation[1]
                        target_pose.pose.orientation.z = temp_orientation[2]
                        target_pose.pose.orientation.w = temp_orientation[3]
                        # target_pose.pose.orientation.x += (loaded_starting_pose[3] - current_pose.pose.orientation.x)/step_t_len

                        self.franka_pose_publisher.publish(target_pose)
                        time.sleep(5/step_t_len) # 5 seconds to reach the starting pose
                        print(step_t)
                    self.timer_back = time.time()
                    self.reach_starting_pose = True
                    print("Back to Task Starting Pose")



            if self.is_active and (time.time() - self.waiting_timer > 0.5):
                target_pose = cp(self.initial_pose)
                print_msg = "\n"

                if (self.translation_demand is not None) and self.check_timer("translation"):
                    print_msg += "Translation  "
                    target_pose.pose.position = cp(self.translation_demand.pose.position)
                    
                if (self.orientation_demand is not None) and self.check_timer("orientation"):
                    print_msg += "Orientation  "
                    target_pose.pose.orientation = cp(self.orientation_demand.pose.orientation)
                    
                    target_pose.pose.position.x += self.orientation_demand.pose.position.x
                    target_pose.pose.position.y += self.orientation_demand.pose.position.y
                    target_pose.pose.position.z += self.orientation_demand.pose.position.z
                    
                print_msg += "Publish!  "
                # COMMENT THIS LINE TO NOT MOVE THE ROBOT!
                self.franka_pose_publisher.publish(target_pose)
                # print_msg += ("\n" + posestamp_message_as_str(target_pose))

                print_msg += ("\n" + posestamp_message_as_str(target_pose))
                self.get_logger().info(print_msg, throttle_duration_sec = 0.1)
            # print_msg += ("\n" + posestamp_message_as_str(target_pose))

def main():
    rclpy.init()
    node = ArmTeleopNode()
    try:
        rclpy.spin(node)
    except KeyboardInterrupt:
        pass

    rclpy.shutdown()


