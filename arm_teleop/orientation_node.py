from geometry_msgs.msg import TransformStamped, PoseStamped
from helper_functions.gamepad_functions import GamepadFunctions

import numpy as np
from sensor_msgs.msg import Joy

from std_msgs.msg import Float32MultiArray
from scipy.spatial.transform import Rotation as R
from scipy.spatial.transform import Slerp

from .teleop_helper_functions import broadcast_tf

import rclpy
from rclpy.node import Node

from copy import deepcopy as cp
import time

from tf2_ros import TransformBroadcaster
from tf2_ros.buffer import Buffer
from tf2_ros.transform_listener import TransformListener


class OrientationNode(Node):

    def __init__(self):
        super().__init__('orientation_node')

        # Gamepad
        self.gp = GamepadFunctions()
        self.gamepad_subscriber = self.create_subscription(Joy, '/gamepad', self.gamepad_callback, 10)

        # tf related
        self.tf_broadcaster = TransformBroadcaster(self)
        self.tf_buffer = Buffer()
        self.tf_listener = TransformListener(self.tf_buffer, self)

        # Main loop
        self.mainloop = self.create_timer(0.001, self.mainloop_callback) 

        # Subscriber
        self.manus_skeleton_subscriber = self.create_subscription(Float32MultiArray, '/manus/imu', self.imu_callback, 10)
        self.franka_cartesian_pose_subscriber = self.create_subscription(PoseStamped, '/cartesian_pose', self.cart_pos_callback, 10)
        
        # Publisher
        self.target_orientation_publisher = self.create_publisher(PoseStamped, '/franka/orientation_demand', 10)

        # Variables
        self.calibration_stage = 0
        self.target_hand_orientation = None
        self.current_franka_pose = None
        self.calibration_timer = cp(time.time())

        # Parameters
        self.DAMPING = 0.95

    def cart_pos_callback(self, msg:PoseStamped):
        self.current_franka_pose = msg


    def imu_callback(self, msg:Float32MultiArray):
        q = msg.data
        
        rotate_z = R.from_euler("z", -45, degrees=True)
        q0 = rotate_z.as_quat()
        broadcast_tf(self, "panda_link8", "temp1", [0.0, 0.0, 0.1], q0)

        rotate_y = R.from_euler("y", 30, degrees=True)
        q1 = rotate_y.as_quat()
        broadcast_tf(self, "temp1", "temp2", [0.0, 0.0, 0.0], q1)

        rotate_z90 = R.from_euler("z", -90, degrees=True)
        q2 = rotate_z90.as_quat()
        broadcast_tf(self, "temp2", "hand_frame", [0.0, 0.0, 0.0], q2)

        # Start of calibration
        if self.calibration_stage == 1 or self.calibration_stage == 2:
            broadcast_tf(self, "hand_frame", "new_base", [0.0, 0.0, 0.0], [q[0], q[1], q[2], q[3]*-1])
            try:
                t = self.tf_buffer.lookup_transform("world", "new_base", rclpy.time.Time())
                ee_to_hand = self.tf_buffer.lookup_transform("hand_frame", "panda_link8", rclpy.time.Time())
                self.ee_to_hand_trans = [ee_to_hand.transform.translation.x, ee_to_hand.transform.translation.y, ee_to_hand.transform.translation.z]
                self.ee_to_hand_ori = [ee_to_hand.transform.rotation.x, ee_to_hand.transform.rotation.y, ee_to_hand.transform.rotation.z, ee_to_hand.transform.rotation.w]
            except:
                print("[Step 1] Waiting for new_base broadcast")
                return
            

            self.world_to_glove_root_trans = [t.transform.translation.x, t.transform.translation.y, t.transform.translation.z]
            self.world_to_glove_root_ori = [t.transform.rotation.x, t.transform.rotation.y, t.transform.rotation.z, t.transform.rotation.w]
            self.target_hand_orientation = cp(q)
            self.static_franka_pose = cp(self.current_franka_pose)
            self.calibration_stage += 1
        

        # Mid calibration
        if self.calibration_stage >= 3:
            broadcast_tf(self, "world", "new_base_wrt_world", self.world_to_glove_root_trans, self.world_to_glove_root_ori)
            broadcast_tf(self, "new_base_wrt_world", "imu_wrt_world", [0.0, 0.0, 0.0], q)
            broadcast_tf(self, "imu_wrt_world", "ee_pos", self.ee_to_hand_trans, self.ee_to_hand_ori)

            # Interpolate hand orientation
            if self.target_hand_orientation is None:
                self.target_hand_orientation = cp(q)
            else:
                # Apply damping by linear interpolation
                slerp = Slerp([0, 1], R.from_quat([q, self.target_hand_orientation]))
                target_R = slerp([self.DAMPING])
                target_hand_orientation = target_R.as_quat()[0]

                self.target_hand_orientation = cp(target_hand_orientation)

                broadcast_tf(self, "new_base_wrt_world", "target_q_wrt_world", [0.0, 0.0, 0.0], target_hand_orientation)
                broadcast_tf(self, "target_q_wrt_world", "ee_pos2", self.ee_to_hand_trans, self.ee_to_hand_ori)

                # get end effector position wrt world
                try:
                    # target_wrt_world = self.tf_buffer.lookup_transform("world", "target_q_wrt_world", rclpy.time.Time())
                    target_wrt_world = self.tf_buffer.lookup_transform("world", "ee_pos2", rclpy.time.Time())
                except:
                    print("[Step 2] Waiting for target_q_wrt_world broadcast")
                    return
                self.calibration_stage = 4

        # If all processes are ready, get the arm position, and update the 
        if self.calibration_stage == 4:
            target_pose = cp(self.current_franka_pose)
            target_pose.pose.position.x = target_wrt_world.transform.translation.x - self.static_franka_pose.pose.position.x
            target_pose.pose.position.y = target_wrt_world.transform.translation.y - self.static_franka_pose.pose.position.y
            target_pose.pose.position.z = target_wrt_world.transform.translation.z - self.static_franka_pose.pose.position.z

            target_pose.pose.orientation.x = target_wrt_world.transform.rotation.x
            target_pose.pose.orientation.y = target_wrt_world.transform.rotation.y
            target_pose.pose.orientation.z = target_wrt_world.transform.rotation.z
            target_pose.pose.orientation.w = target_wrt_world.transform.rotation.w

            self.target_orientation_publisher.publish(target_pose)
            
            print([round(target_pose.pose.position.x, 4), round(target_pose.pose.position.y, 4), round(target_pose.pose.position.z, 4)])
    def gamepad_callback(self, gamepad_raw_msg:Joy):
        self.gp.convert_joy_msg_to_dictionary(gamepad_raw_msg)

    def mainloop_callback(self):
        if self.gp.button_data["L1"] == True and self.calibration_stage == 0 and (self.current_franka_pose is not None):
            self.calibration_stage = 1
            
        if self.gp.button_data["L1"] == False and self.calibration_stage > 0:
            self.calibration_stage = 0


def main():
    rclpy.init()
    node = OrientationNode()
    try:
        rclpy.spin(node)
    except KeyboardInterrupt:
        pass

    rclpy.shutdown()


